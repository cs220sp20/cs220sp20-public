# EN.601.220, Spring 2020, at Johns Hopkins University

This is the public repository for EN.601.220, Intermediate Programming,
Spring 2020, at Johns Hopkins University.

This repository will be used to distribute public code resources
such as exercise and homework starter code.

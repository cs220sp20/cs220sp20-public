#include <iostream>
#include <vector>
using std::cout; using std::endl; using std::vector;
 
bool has_dups(vector<char> vec) {
  // TODO: complete the function to return true if
  // vec has duplicates, false otherwise
  return false; // STUB: Replace this
}
 
int main() {
  vector<char> v;
  for (char c = 'a'; c < 'i'; c++) v.push_back(c);
  v.insert(v.begin() + 2, 'f');
  cout << "v's content:" << endl;
  for (char c: v) cout << c << " ";
  cout << endl;
  cout << "does v have duplicates? " << has_dups(v) << endl;
  return 0;
}

//Ex2-1: gpa_simple.c

/* The purpose of this program is to compute GPAs for simple letter
   grades - no +/-, only A, B, C, D, F. Credits may be rational
   numbers.  Also determine and display notices for Dean's List
   (>=3.5) and Academic Probation (< 2.0).

SAMPLE RUN:

Welcome to the GPA calculator!
Enter grade and credits for each course below (ctrl-d to end):
course 1: A 4.0
course 2: b 2.7
course 3: B 3.5
course 4: c 3.0
course 5: f 1
course 6: a 3
course 7: 
Your GPA is 3.06

-----
PSEUDOCODE:

point_sum gets 0
credit_sum gets 0
points gets 0
gpa gets 0

display “Welcome to GPA calculator!”
prompt for list of grade/credits pairs

count gets 1
display "course #", count

repeat while there is a grade and credits to read
    convert grade to points
    add points * credits to point_sum
    add credits to credit_sum
    add 1 to count
    display "course #", count

if credit_sum > 0
   set gpa to point_sum / credit_sum
   display "GPA is ", gpa
   if gpa >= 3.5
      display "Dean's List"
   otherwise if gpa <= 2.0
      display "Uh-oh, Academic Probation..."
otherwise
   display "No credits attempted; no GPA to report"

*/


#include <stdio.h>

int main() {

  //TO DO: add your code here so that your program
  //produces output that would be identical to the
  //given sample run when provided the same input
  char grade;
  float credits;
  float total_credits = 0.0f;
  float quality_pts = 0.0f;
  int count = 1;

  printf("Welcome to the GPA calculator!\n");
  printf("Enter grade and credits for each course below (ctrl-d to end):\n");

  // read pairs of grade letter, number of credits
  printf("Course %d: ", count);
  while (scanf(" %c %f", &grade, &credits) == 2) {
    count++;
    // convert grade to gpa scale
    float scale;
    switch (grade) {
    case 'a':
    case 'A':
      scale = 4.0f;
      break;
    case 'b':
    case 'B':
      scale = 3.0f;
      break;
    case 'c':
    case 'C':
      scale = 2.0f;
      break;
    case 'd':
    case 'D':
      scale = 1.0f;
      break;
    case 'f':
    case 'F':
      scale = 0.0f;
      break;
    default:
      printf("Unrecognized grade!\n");
      scale = 0.0f;
    }
    // update total quality points and total number of credits
    quality_pts += (scale * credits);
    total_credits += credits;
    // repeat prompt
    printf("Course %d: ", count);
  }

  if (credits > 0.0f) {
    float gpa = quality_pts / total_credits;
    // display "GPA is ", gpa
    printf("\nGPA is %.02f\n", gpa);
    if (gpa >= 3.5) {
      printf("Dean's list\n");
    } else if (gpa <= 2.0) {
      printf("Uh-oh, Academic Probation...\n");
    }
  } else {
    printf("No credits attempted; no GPA to report\n");
  }

  return 0;
}
